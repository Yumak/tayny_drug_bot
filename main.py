import telebot

TOKEN = "1481148706:AAGFhBdukLukiG4zaIijcMPSskppVT43ZAQ"
MY_ID = 634041386  # 1093110311  #
DRUG_ID = 419275005  # 866402698  #

bot = telebot.TeleBot(token=TOKEN, parse_mode="MARKDOWN")
print("Start bot...")


@bot.message_handler(commands=['start', 'help'])
def command_start_help(message):
    print(message)
    if message.from_user.id == MY_ID:
        bot.send_message(chat_id=message.chat.id,
                         text='🖐')
        bot.send_message(chat_id=message.chat.id,
                         text=f'Привет, *{message.from_user.first_name}*, это бот для общения с твоим "Тайным Другом".\n'
                              'Через меня можно передать любой тип сообщения из: '
                              'Текстовое, Фото, Видео, Аудио, Голосовое или можно отправить Документ\n'
                              '*Всего хорошего*')
    elif message.from_user.id == DRUG_ID:
        bot.send_message(chat_id=message.chat.id,
                         text='🖐')
        bot.send_message(chat_id=message.chat.id,
                         text=f'Привет, *{message.from_user.first_name}*, я Твой "Тайный Друг".\n'
                              f'Чтобы со мной общатся, пиши этому боту сообщения, и он их передаст мне.\n'
                              f'*Всего хорошего*')
    else:
        bot.send_message(chat_id=message.chat.id,
                         text='🖐')
        bot.send_message(chat_id=message.chat.id,
                         text=f'Привет, *{message.from_user.first_name}*, этот бот работает только '
                              f'с определенным кругом людей, Извини пожалуйста.\n'
                              f'*Всего хорошего*')


@bot.message_handler(content_types=['text'])
def message_text(message):
    if message.from_user.id == MY_ID:
        ChatId = DRUG_ID
    elif message.from_user.id == DRUG_ID:
        ChatId = MY_ID
    else:
        bot.send_message(chat_id=message.from_user.id,
                         text=f"{message.from_user.first_name}, Извини, я с тобой не могу работать.\n"
                              f"*'Свыше'* не позволено.")
        return
    try:
        bot.send_message(chat_id=ChatId,
                         text=message.text)
    except telebot.ExceptionHandler as error:
        print(error)


@bot.message_handler(content_types=['photo'])
def message_photo(message):
    print(message)
    if message.from_user.id == MY_ID:
        ChatId = DRUG_ID
    elif message.from_user.id == DRUG_ID:
        ChatId = MY_ID
    else:
        bot.send_message(chat_id=message.from_user.id,
                         text=f"{message.from_user.first_name}, Извини, я с тобой не могу работать.\n"
                              f"*'Свыше'* не позволено.")
        return
    bot.send_photo(chat_id=ChatId,
                   photo=message.photo[-1].file_id,
                   caption=message.caption)


@bot.message_handler(content_types=['video'])
def message_video(message):
    print(message)
    if message.from_user.id == MY_ID:
        ChatId = DRUG_ID
    elif message.from_user.id == DRUG_ID:
        ChatId = MY_ID
    else:
        bot.send_message(chat_id=message.from_user.id,
                         text=f"{message.from_user.first_name}, Извини, я с тобой не могу работать.\n"
                              f"*'Свыше'* не позволено.")
        return
    bot.send_video(chat_id=ChatId,
                   data=message.video.file_id,
                   caption=message.caption)


@bot.message_handler(content_types=['audio'])
def message_audio(message):
    print(message)
    if message.from_user.id == MY_ID:
        ChatId = DRUG_ID
    elif message.from_user.id == DRUG_ID:
        ChatId = MY_ID
    else:
        bot.send_message(chat_id=message.from_user.id,
                         text=f"{message.from_user.first_name}, Извини, я с тобой не могу работать.\n"
                              f"*'Свыше'* не позволено.")
        return
    bot.send_audio(chat_id=ChatId,
                   audio=message.audio.file_id,
                   caption=message.caption)


@bot.message_handler(content_types=['voice'])
def message_voice(message):
    print(message)
    if message.from_user.id == MY_ID:
        ChatId = DRUG_ID
    elif message.from_user.id == DRUG_ID:
        ChatId = MY_ID
    else:
        bot.send_message(chat_id=message.from_user.id,
                         text=f"{message.from_user.first_name}, Извини, я с тобой не могу работать.\n"
                              f"*'Свыше'* не позволено.")
        return
    bot.send_voice(chat_id=ChatId,
                   voice=message.voice.file_id)


@bot.message_handler(content_types=['document'])
def message_document(message):
    print(message)
    if message.from_user.id == MY_ID:
        ChatId = DRUG_ID
    elif message.from_user.id == DRUG_ID:
        ChatId = MY_ID
    else:
        bot.send_message(chat_id=message.from_user.id,
                         text=f"{message.from_user.first_name}, Извини, я с тобой не могу работать.\n"
                              f"*'Свыше'* не позволено.")
        return
    bot.send_document(chat_id=ChatId,
                      data=message.document.file_id,
                      caption=message.caption)


bot.polling(none_stop=False, interval=0, timeout=20)
